# README #

### What is this repository for? ###

* This repository contains edited version of the HorribleSubs English subtitles for the Steins;Gate 0 anime. 
* The changes that have been made to the subs were mainly made for reasons of consistency with the Steins;Gate 0 Visual Novel
* They include things like "Luka" instead of "Ruka" as it is the official translation from the authors

### Who do I talk to? ###

* You can contact Woute#4407 or Davixxa#4194 on Discord for any inquiry